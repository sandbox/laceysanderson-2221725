<?php

class views_handler_field_group_by_string_list extends views_handler_field_prerender_list {

  function pre_render(&$values) {
    parent::pre_render($values);

    $field = $this->field_alias;
    foreach($values as $k => $v) {
      if (preg_match('/{.*}/',$v->{$field})) {
        $this->items[$k] = $this->split_agg_array_results($v->{$field});
      }
      else {
        $this->items[$k] = array($v->{$field});
      }
      $values[$k]->{$field} = $this->items[$k];

      // If the overriden class extends prerender_list as well
      // then we want to let overridden class render it
      if ($this->overrrides_prerender_list) {

      }
      // Otherwise, we want to use the overriden class to render each part
      // but need to take care of rendering it as a list ourselves
      else {

      }
    }

  }

  function render_item($count, $item) {
  }

  function allow_advanced_render() {
    return TRUE;
  }

  /**
   * Splits an PostgreSQL array of results in a PHP array
   *
   * @param $field
   *   An SQL array (ie: {"",^(.*)$,646,tripal_analysis_blast} )
   * @return
   *   A PHP version of the SQL array (ie: array('','^(.*)$','646','tripal_analysis_blast') )
   */
  function split_agg_array_results($field) {

  	if (is_array($field)) {
  		return $field;
  	}

    if(preg_match('/^{(.*)}$/',$field, $matches)) {
      return str_getcsv($matches[1]);
    } else {
      return array();
    }

    return $values;
  }

  /**
   * Added for debugging
   *
  function get_items($values) {
    // Only the parent get_value returns a single field.
    $field = parent::get_value($values);
    ddl($field, 'field in get items');
    if (!empty($this->items[$field])) {
      return $this->items[$field];
    }

    return array();
  }
*/
  /**
   * added for debugging
   */
  function advanced_render($values) {

    $field = parent::get_value($values);

  if ($this->allow_advanced_render() && method_exists($this, 'render_item')) {
    $raw_items = $this->get_items($values);
    // If there are no items, set the original value to NULL.
    if (empty($raw_items)) {
      $this->original_value = NULL;
    }
  }
  else {
    $value = $this->render($values);
    if (is_array($value)) {
      $value = drupal_render($value);
    }
    $this->last_render = $value;
    $this->original_value = $value;
  }

  if ($this->allow_advanced_render()) {
    $tokens = NULL;
    if (method_exists($this, 'render_item')) {
      $items = array();
      foreach ($raw_items as $count => $item) {
        $value = $this->render_item($count, $item);
        if (is_array($value)) {
          $value = drupal_render($value);
        }
        $this->last_render = $value;
        $this->original_value = $this->last_render;

        $alter = $item + $this->options['alter'];
        $alter['phase'] = VIEWS_HANDLER_RENDER_TEXT_PHASE_SINGLE_ITEM;
        $items[] = $this->render_text($alter);
      }

      $value = $this->render_items($items);
    }
    else {
      $alter = array('phase' => VIEWS_HANDLER_RENDER_TEXT_PHASE_COMPLETELY) + $this->options['alter'];
      $value = $this->render_text($alter);
    }

    if (is_array($value)) {
      $value = drupal_render($value);
    }
    // This happens here so that render_as_link can get the unaltered value of
    // this field as a token rather than the altered value.
    $this->last_render = $value;
  }

  if (empty($this->last_render)) {
    if ($this->is_value_empty($this->last_render, $this->options['empty_zero'], FALSE)) {
      $alter = $this->options['alter'];
      $alter['alter_text'] = 1;
      $alter['text'] = $this->options['empty'];
      $alter['phase'] = VIEWS_HANDLER_RENDER_TEXT_PHASE_EMPTY;
      $this->last_render = $this->render_text($alter);
    }
  }

  return $this->last_render;

    //return parent::advanced_render($values);
  }

  /**
   * {@inheritdoc}
   */
  function init(&$view, $options) {
    $orginal_handler = $this->definition['handler'];
    $orginal_handler::init($view, $options);

    // Check if the handler this one overrides renders lists.
    if (method_exists($this->definition['handler'], 'render_item')) {
      $this->overrrides_prerender_list = TRUE;
    }
    else {
      $this->overrrides_prerender_list = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  function options_form(&$form, &$form_state) {
    $orginal_handler = $this->definition['handler'];
    $orginal_handler::options_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  function option_submit($form, $form_state) {
    $orginal_handler = $this->definition['handler'];
    $orginal_handler::option_submit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  function query() {
    $orginal_handler = $this->definition['handler'];
    $orginal_handler::query();
  }
}